"""

    Author: Evelyn Asmat

    'IPS' is a dictionary
    where the keys are the IP adresses that have made a request and
    the values are the number of times the IP address have made that request.

    If I had more time, I would use an external library such as Numpy
    to have better performance and develop some unit tests.

    I decided not to use lists because lookups are faster in dictionaries
    as Python implements them using hash tables.

    It can be tested by generating random ip_adresses and implementing
    functional and unit tests.


"""

IPS = {}


def request_handled(ip_address):
    """

    Checks if the ip_address is already in the dictionary IPS,
    if so, it adds 1 to the value.
    If not, it is added to the dictionary with a value of 1.

    Runtime complexity: O(1)

    """

    IPS[ip_address] = IPS.get(ip_address, 0) + 1


def top100():
    """

    Gets the top100 IPs occurrences, by sorting the values in descending order.

    Runtime complexity: O(n log n)

    """

    return sorted(IPS.iterkeys(), key=lambda k: IPS[k], reverse=True)[:100]


def clear():
    """

    Clears the dictionary IPS.

    Runtime complexity: O(1)

    """

    IPS.clear()
